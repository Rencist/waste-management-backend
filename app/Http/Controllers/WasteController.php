<?php

namespace App\Http\Controllers;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Core\Application\Service\CreateWaste\CreateWasteRequest;
use App\Core\Application\Service\CreateWaste\CreateWasteService;
use App\Core\Application\Service\DeleteWaste\DeleteWasteService;
use App\Core\Application\Service\GetTypeList\GetTypeListService;
use App\Core\Application\Service\GetWasteList\GetWasteListService;

class WasteController extends Controller
{
    public function createWaste(Request $request, CreateWasteService $service): JsonResponse
    {
        $request->validate([
            'type_id' => 'required',
            'name' => 'required',
        ]);

        $input = new CreateWasteRequest(
            $request->input('type_id'),
            $request->input('name'),
        );

        DB::beginTransaction();
        try {
            $service->execute($input, $request->get('account'));
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();
        return $this->success("Berhasil Menambahkan Sampah");
    }

    public function getWasteList(Request $request, GetWasteListService $service): JsonResponse
    {
        $response = $service->execute($request->get('account'));
        return $this->successWithData($response, "Berhasil Mendapatkan List Sampah");
    }
    
    public function deleteWaste(Request $request, DeleteWasteService $service): JsonResponse
    {
        DB::beginTransaction();
        try {
            $service->execute($request->input('waste_id'), $request->get('account'));
        } catch (Throwable $e) {
            DB::rollBack();
            throw $e;
        }
        DB::commit();
        
        return $this->success("Berhasil Menghapus Sampah");
    }

    public function getTypeList(GetTypeListService $service): JsonResponse
    {
        $response = $service->execute();
        return $this->successWithData($response, "Berhasil Mendapatkan List Sampah");
    }
}
