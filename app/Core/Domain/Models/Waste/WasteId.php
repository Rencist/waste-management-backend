<?php

namespace App\Core\Domain\Models\Waste;

use App\Core\Domain\Models\UuidTrait;

class WasteId
{
    use UuidTrait;
}
