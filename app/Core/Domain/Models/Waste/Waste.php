<?php

namespace App\Core\Domain\Models\Waste;

use App\Core\Domain\Models\User\UserId;
use Exception;
use App\Core\Domain\Models\Waste\WasteId;

class Waste
{
    private WasteId $id;
    private UserId $user_Id;
    private string $type_id;
    private string $name;

    /**
     * @param WasteId $id
     * @param UserId $user_Id
     * @param string $type_id
     * @param string $name
     */
    public function __construct(WasteId $id, UserId $user_Id, string $type_id, string $name)
    {
        $this->id = $id;
        $this->user_Id = $user_Id;
        $this->type_id = $type_id;
        $this->name = $name;
    }

    /**
     * @throws Exception
     */
    public static function create(UserId $user_Id, string $type_id, string $name): self
    {
        return new self(
            WasteId::generate(),
            $user_Id,
            $type_id,
            $name,
        );
    }

    /**
     * @return WasteId
     */
    public function getId(): WasteId
    {
        return $this->id;
    }

    /**
     * @return UserId
     */
    public function getUserId(): UserId
    {
        return $this->user_Id;
    }

    /**
     * @return string
     */
    public function getTypeId(): string
    {
        return $this->type_id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
