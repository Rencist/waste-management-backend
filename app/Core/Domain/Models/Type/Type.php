<?php

namespace App\Core\Domain\Models\Type;

use Exception;

class Type
{
    private string $id;
    private string $material_id;
    private string $name;

    /**
     * @param string $id
     * @param string $material_id
     * @param string $name
     */
    public function __construct(string $id, string $material_id, string $name)
    {
        $this->id = $id;
        $this->material_id = $material_id;
        $this->name = $name;
    }

    /**
     * @throws Exception
     */
    public static function create(string $id, string $material_id, string $name): self
    {
        return new self(
            $id,
            $material_id,
            $name,
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */

    public function getMaterialId(): string
    {
        return $this->material_id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
