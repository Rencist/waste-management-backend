<?php

namespace App\Core\Domain\Models\Material;

use Exception;

class Material
{
    private string $id;
    private string $name;

    /**
     * @param string $id
     * @param string $name
     */
    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @throws Exception
     */
    public static function create(string $name, string $id): self
    {
        return new self(
            $id,
            $name,
        );
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
