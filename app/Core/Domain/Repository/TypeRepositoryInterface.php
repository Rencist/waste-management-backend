<?php

namespace App\Core\Domain\Repository;

use App\Core\Domain\Models\Type\Type;

interface TypeRepositoryInterface
{
    public function persist(Type $Type): void;

    public function find(string $type_id): ?Type;

    public function getAll(): array;
}
