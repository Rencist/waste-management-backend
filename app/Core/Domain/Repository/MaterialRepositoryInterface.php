<?php

namespace App\Core\Domain\Repository;

use App\Core\Domain\Models\Material\Material;

interface MaterialRepositoryInterface
{
    public function persist(Material $Material): void;

    public function find(string $material_id): ?Material;
}
