<?php

namespace App\Core\Domain\Repository;

use App\Core\Domain\Models\User\UserId;
use App\Core\Domain\Models\Waste\Waste;
use App\Core\Domain\Models\Waste\WasteId;

interface WasteRepositoryInterface
{
    public function persist(Waste $Waste): void;

    public function find(WasteId $id): ?Waste;

    public function delete(WasteId $id): void;

    public function findByUserId(UserId $id): ?array;
}
