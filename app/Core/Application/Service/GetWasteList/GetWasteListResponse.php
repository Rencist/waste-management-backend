<?php

namespace App\Core\Application\Service\GetWasteList;

use JsonSerializable;

class GetWasteListResponse implements JsonSerializable
{
    private string $id;
    private string $material;
    private string $type;
    private string $waste;

    /**
     * @param string $id
     * @param string $material
     * @param string $type
     * @param string $waste
     */
    public function __construct(string $id, string $material, string $type, string $waste)
    {
        $this->id = $id;
        $this->material = $material;
        $this->type = $type;
        $this->waste = $waste;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'material' => $this->material,
            'type' => $this->type,
            'waste' => $this->waste,
        ];
    }
}
