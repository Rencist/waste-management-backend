<?php

namespace App\Core\Application\Service\GetWasteList;

use App\Core\Domain\Models\UserAccount;
use Exception;
use App\Core\Domain\Models\Waste\Waste;
use App\Core\Domain\Repository\MaterialRepositoryInterface;
use App\Core\Domain\Repository\TypeRepositoryInterface;
use App\Core\Domain\Repository\WasteRepositoryInterface;

class GetWasteListService
{
    private WasteRepositoryInterface $waste_repository;
    private TypeRepositoryInterface $type_repository;
    private MaterialRepositoryInterface $material_repository;

    /**
     * @param WasteRepositoryInterface $waste_repository
     * @param TypeRepositoryInterface $type_repository
     * @param MaterialRepositoryInterface $material_repository
     */
    public function __construct(WasteRepositoryInterface $waste_repository, TypeRepositoryInterface $type_repository, MaterialRepositoryInterface $material_repository)
    {
        $this->waste_repository = $waste_repository;
        $this->type_repository = $type_repository;
        $this->material_repository = $material_repository;
    }

    /**
     * @throws Exception
     */
    public function execute(UserAccount $user_account)
    {
        $waste_list = $this->waste_repository->findByUserId($user_account->getUserId());
        return array_map(function (Waste $waste){
            $type = $this->type_repository->find($waste->getTypeId());
            $material = $this->material_repository->find($type->getMaterialId());
            return new GetWasteListResponse(
                $waste->getId()->toString(),
                $material->getName(),
                $type->getName(),
                $waste->getName(),
            );
        }, $waste_list);
    }
}
