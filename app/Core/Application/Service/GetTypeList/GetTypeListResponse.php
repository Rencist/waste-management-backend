<?php

namespace App\Core\Application\Service\GetTypeList;

use JsonSerializable;

class GetTypeListResponse implements JsonSerializable
{
    private string $id;
    private string $material;
    private string $name;

    /**
     * @param string $id
     * @param string $material
     * @param string $name
     */
    public function __construct(string $id, string $material, string $name)
    {
        $this->id = $id;
        $this->material = $material;
        $this->name = $name;
    }

    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'material' => $this->material,
            'name' => $this->name,
        ];
    }
}
