<?php

namespace App\Core\Application\Service\GetTypeList;

use App\Core\Domain\Models\Type\Type;
use App\Core\Domain\Models\UserAccount;
use Exception;
use App\Core\Domain\Models\Waste\Waste;
use App\Core\Domain\Repository\MaterialRepositoryInterface;
use App\Core\Domain\Repository\TypeRepositoryInterface;
use App\Core\Domain\Repository\WasteRepositoryInterface;

class GetTypeListService
{
    private TypeRepositoryInterface $type_repository;
    private MaterialRepositoryInterface $material_repository;

    /**
     * @param TypeRepositoryInterface $type_repository
     * @param MaterialRepositoryInterface $material_repository
     */
    public function __construct(TypeRepositoryInterface $type_repository, MaterialRepositoryInterface $material_repository)
    {
        $this->type_repository = $type_repository;
        $this->material_repository = $material_repository;
    }

    /**
     * @throws Exception
     */
    public function execute()
    {
        $type_list = $this->type_repository->getAll();
        return array_map(function (Type $type){
            $material = $this->material_repository->find($type->getMaterialId());
            return new GetTypeListResponse(
                $type->getId(),
                $material->getName(),
                $type->getName(),
            );
        }, $type_list);
    }
}
