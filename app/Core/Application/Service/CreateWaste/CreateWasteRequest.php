<?php

namespace App\Core\Application\Service\CreateWaste;

class CreateWasteRequest
{
    private string $type_id;
    private string $name;

    /**
     * @param string $type_id
     * @param string $name
     */
    public function __construct(string $type_id, string $name)
    {
        $this->type_id = $type_id;
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getTypeId(): string
    {
        return $this->type_id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
