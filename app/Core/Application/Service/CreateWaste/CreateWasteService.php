<?php

namespace App\Core\Application\Service\CreateWaste;

use App\Core\Domain\Models\UserAccount;
use Exception;
use App\Core\Domain\Models\Waste\Waste;
use App\Core\Domain\Repository\WasteRepositoryInterface;
use App\Core\Domain\Repository\AccountVerificationRepositoryInterface;

class CreateWasteService
{
    private WasteRepositoryInterface $waste_repository;

    /**
     * @param WasteRepositoryInterface $waste_repository
     * @param AccountVerificationRepositoryInterface $account_verification_repository
     */
    public function __construct(WasteRepositoryInterface $waste_repository)
    {
        $this->waste_repository = $waste_repository;
    }

    /**
     * @throws Exception
     */
    public function execute(CreateWasteRequest $request, UserAccount $user_account)
    {
        $waste = Waste::create(
            $user_account->getUserId(),
            $request->getTypeId(),
            $request->getName(),
        );
        $this->waste_repository->persist($waste);
    }
}
