<?php

namespace App\Core\Application\Service\DeleteWaste;

use App\Core\Domain\Models\UserAccount;
use App\Core\Domain\Models\Waste\WasteId;
use App\Core\Domain\Repository\UserRepositoryInterface;
use Exception;
use App\Exceptions\UserException;
use App\Core\Domain\Repository\WasteRepositoryInterface;

class DeleteWasteService
{
    private WasteRepositoryInterface $waste_repository;
    private UserRepositoryInterface $user_repository;

    /**
     * @param WasteRepositoryInterface $waste_repository
     * @param UserRepositoryInterface $user_repository
     */

    public function __construct(WasteRepositoryInterface $waste_repository, UserRepositoryInterface $user_repository)
    {
        $this->waste_repository = $waste_repository;
        $this->user_repository = $user_repository;
    }

    /**
     * @throws Exception
     */
    public function execute(string $waste_id, UserAccount $user_account)
    {
        $user = $this->user_repository->find($user_account->getUserId());
        if (!$user) {
            UserException::throw("User Tidak Ditemukan", 1008, 400);
        }
        $waste = $this->waste_repository->find(new WasteId($waste_id));
        if (!$waste) {
            UserException::throw("Sampah Tidak Ditemukan", 1008, 400);
        }
        if ($waste->getUserId() != $user->getId()) {
            UserException::throw("Sampah Tidak Ditemukan", 1008, 400);
        }
        $this->waste_repository->delete($waste->getId());
    }
}
