<?php

namespace App\Infrastrucutre\Repository;

use App\Core\Domain\Models\Email;
use App\Core\Domain\Models\User\UserId;
use App\Core\Domain\Models\Type\Type;
use App\Core\Domain\Models\Type\TypeId;
use App\Core\Domain\Repository\TypeRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class SqlTypeRepository implements TypeRepositoryInterface
{
    public function persist(Type $types): void
    {
        DB::table('types')->upsert([
            'id' => $types->getId(),
            'materials_id' => $types->getMaterialId(),
            'name' => $types->getName(),
        ], 'id');
    }

    /**
     * @throws Exception
     */
    public function find(string $type_id): ?Type
    {
        $rows = DB::table('types')->where('id', $type_id)->first();

        if (!$rows) {
            return null;
        }
        
        return $this->constructFromRows([$rows])[0];
    }

    public function getAll(): array
    {
        $rows = DB::table('types')->get();
        return $this->constructFromRows($rows->all());
    }

    /**
     * @throws Exception
     */
    public function constructFromRows(array $rows): array
    {
        $types = [];
        foreach ($rows as $row) {
            $types[] = new Type(
                $row->id,
                $row->materials_id,
                $row->name,
            );
        }
        return $types;
    }
}
