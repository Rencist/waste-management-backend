<?php

namespace App\Infrastrucutre\Repository;

use App\Core\Domain\Models\Email;
use App\Core\Domain\Models\User\UserId;
use App\Core\Domain\Models\Waste\Waste;
use App\Core\Domain\Models\Waste\WasteId;
use App\Core\Domain\Repository\WasteRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class SqlWasteRepository implements WasteRepositoryInterface
{
    public function persist(Waste $wastes): void
    {
        DB::table('wastes')->upsert([
            'id' => $wastes->getId()->toString(),
            'users_id' => $wastes->getUserId()->toString(),
            'types_id' => $wastes->getTypeId(),
            'name' => $wastes->getName(),
        ], 'id');
    }

    /**
     * @throws Exception
     */
    public function find(WasteId $id): ?Waste
    {
        $rows = DB::table('wastes')->where('id', $id->toString())->first();

        if (!$rows) {
            return null;
        }

        return $this->constructFromRows([$rows])[0];
    }

    public function delete(WasteId $id): void
    {
        DB::table('wastes')->where('id', $id->toString())->delete();
    }

    /**
     * @throws Exception
     */
    public function findByUserId(UserId $user_id): ?array
    {
        $rows = DB::table('wastes')->where('users_id', $user_id->toString())->get();

        if (!$rows) {
            return null;
        }

        return $this->constructFromRows($rows->all());
    }

    /**
     * @throws Exception
     */
    public function constructFromRows(array $rows): array
    {
        $wastes = [];
        foreach ($rows as $row) {
            $wastes[] = new Waste(
                new WasteId($row->id),
                new UserId($row->users_id),
                $row->types_id,
                $row->name,
            );
        }
        return $wastes;
    }
}
