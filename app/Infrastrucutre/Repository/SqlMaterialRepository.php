<?php

namespace App\Infrastrucutre\Repository;

use App\Core\Domain\Models\Material\Material;
use App\Core\Domain\Repository\MaterialRepositoryInterface;
use Exception;
use Illuminate\Support\Facades\DB;

class SqlMaterialRepository implements MaterialRepositoryInterface
{
    public function persist(Material $materials): void
    {
        DB::table('materials')->upsert([
            'id' => $materials->getId(),
            'name' => $materials->getName(),
        ], 'id');
    }

    /**
     * @throws Exception
     */
    public function find(string $material_id): ?Material
    {
        $rows = DB::table('materials')->where('id', $material_id)->first();

        if (!$rows) {
            return null;
        }

        return $this->constructFromRows([$rows])[0];
    }

    /**
     * @throws Exception
     */
    public function constructFromRows(array $rows): array
    {
        $materials = [];
        foreach ($rows as $row) {
            $materials[] = new Material(
                $row->id,
                $row->name,
            );
        }
        return $materials;
    }
}
