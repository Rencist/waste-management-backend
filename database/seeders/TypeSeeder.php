<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents(database_path('seeders/json/type.json'));
        $types = json_decode($json, true);

        $payload = [];
        foreach ($types as $type) {
            $payload[] = [
                'id' => $type['id'],
                'materials_id' => $type['materials_id'],
                'name' => $type['name']
            ];
        }
        DB::table('types')->insert($payload);
    }
}
