<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MaterialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $json = file_get_contents(database_path('seeders/json/material.json'));
        $materials = json_decode($json, true);

        $payload = [];
        foreach ($materials as $material) {
            $payload[] = [
                'id' => $material['id'],
                'name' => $material['name']
            ];
        }
        DB::table('materials')->insert($payload);
    }
}
