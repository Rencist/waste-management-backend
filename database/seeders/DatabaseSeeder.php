<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Database\Seeders\CitySeeder;
use Database\Seeders\RoleSeeder;
use Database\Seeders\TypeSeeder;
use Database\Seeders\UserSeeder;
use Database\Seeders\VillageSeeder;
use Database\Seeders\MaterialSeeder;
use Database\Seeders\ProvinceSeeder;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\SubdistrictSeeder;
use Database\Seeders\RoleHasPermissionSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            ProvinceSeeder::class,
            CitySeeder::class,
            SubdistrictSeeder::class,
            VillageSeeder::class,
            RoleSeeder::class,
            PermissionSeeder::class,
            RoleHasPermissionSeeder::class,
            UserSeeder::class,
            MaterialSeeder::class,
            TypeSeeder::class,
        ]);
    }
}
